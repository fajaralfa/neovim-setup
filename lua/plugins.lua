local plug = vim.fn['plug#']

vim.call('plug#begin')

plug 'EdenEast/nightfox.nvim'
plug 'preservim/nerdtree'
plug 'nvim-tree/nvim-web-devicons'
plug 'folke/trouble.nvim'

-- Code Completion Engine
plug 'neovim/nvim-lspconfig'
plug 'hrsh7th/cmp-nvim-lsp'
plug 'hrsh7th/cmp-buffer'
plug 'hrsh7th/cmp-path'
plug 'hrsh7th/cmp-cmdline'
plug 'hrsh7th/nvim-cmp'
plug 'hrsh7th/cmp-vsnip'
plug 'hrsh7th/vim-vsnip'
plug 'windwp/nvim-autopairs'

vim.call('plug#end')

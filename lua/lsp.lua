local lsp = require'lspconfig'
local capabilities = require('cmp_nvim_lsp').default_capabilities()

local lspserver = {
    'pyright', 'tsserver', 'intelephense', 'ccls', 'lua_ls',
    'intelephense', 'html', 'emmet_ls',
}

for index, value in ipairs(lspserver) do
    lsp[value].setup{ capabilities = capabilities }
end

local options = {
    mouse = 'a',
    expandtab = true,
    softtabstop = 4,
    shiftwidth = 4,
    autoindent = true,
    number = true,
    relativenumber = true,
}

vim.cmd('colorscheme carbonfox')

for k, v in pairs(options) do
    vim.opt[k] = v
end

